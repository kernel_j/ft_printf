# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/22 10:37:17 by jwong             #+#    #+#              #
#    Updated: 2016/04/20 16:19:27 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= libftprintf.a

SRC		= ft_add_sign.c				\
		  ft_alignment1.c			\
		  ft_alignment2.c			\
		  ft_atoi.c					\
		  ft_bzero.c				\
		  ft_cleanup.c				\
		  ft_fill_buffer.c			\
		  ft_fprintf.c				\
		  ft_format1.c				\
		  ft_format2.c				\
		  ft_format_processing.c	\
		  ft_itoa_base.c			\
		  ft_isdigit.c				\
		  ft_isvalid_lmod.c			\
		  ft_justified.c			\
		  ft_memcpy.c				\
		  ft_precision_handling.c	\
		  ft_preformatting.c		\
		  ft_printf.c				\
		  ft_remalloc.c				\
		  ft_snprintf.c				\
		  ft_specifiers1.c			\
		  ft_specifiers2.c			\
		  ft_sprintf.c				\
		  ft_store_options.c		\
		  ft_strcmp.c				\
		  ft_strdup.c				\
		  ft_strjoin.c				\
		  ft_strlen.c				\
		  ft_strncmp.c				\
		  ft_strsub.c				\
		  ft_vfprintf.c				\
		  ft_vprintf.c				\
		  ft_vsnprintf.c			\
		  ft_vsprintf.c				\
		  ft_wchar_processing.c		\
		  ft_wstring_processing.c

CC		= gcc
CFLAGS	= -Wall -Werror -Wextra 
OBJ		= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		ar rc $(NAME) $(OBJ)
		ranlib $(NAME)

%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: fclean all
